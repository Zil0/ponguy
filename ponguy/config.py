from ponguy.default_config import *  # pylint: disable=wildcard-import,unused-wildcard-import

try:
    from ponguy.config_override import *  # pylint: disable=wildcard-import
except ImportError:
    pass
