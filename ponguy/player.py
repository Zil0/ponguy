from random import randint

from ponguy import config


class Player:
    initial_life_points = config.POINTS_TO_WIN
    hitting_range = config.HITTING_RANGE
    button_state = 0
    life_points = 0
    is_a_bot = False
    pressing = False
    bonus = None

    def __init__(self, button_pin, color, position, direction, is_a_bot=False):
        self.button_pin = button_pin
        self.color = color
        self.position = position
        self.direction = direction
        self.is_a_bot = is_a_bot
        self.first_hit_position = position + direction * (config.HITTING_RANGE - 1)

    def init_game(self):
        self.life_points = self.initial_life_points

    def holding(self):
        return bool(self.button_pin.value() == 0)

    def check_button_press(self):
        self.pressing = self.check_for_pressing()

    def check_for_pressing(self):
        value = self.button_pin.value()

        if value == 0:
            self.is_a_bot = False

        if self.is_a_bot:
            return True

        if self.button_state == value:
            return False

        self.button_state = value
        return bool(value == 0)

    def send_back(self, ball):
        min_position = self.position if self.direction == 1 else self.first_hit_position
        max_position = self.first_hit_position if self.direction == 1 else self.position
        if not ball.is_in_range(min_position, max_position):
            if (
                self.pressing
                and not self.is_a_bot
                and (not self.bonus or not self.bonus.auto_send_back)
                and ball.is_in_range(min_position - self.hitting_range, max_position + self.hitting_range)
            ):
                ball.speed = 0
            return False

        if self.bonus and self.bonus.auto_send_back:
            ball.speed = ball.perfect_hit_speed
            ball.plain_trail = True
            return True

        if not self.pressing:
            return False

        if self.is_a_bot:
            ball.set_position(randint(min_position, max_position))

        ball.plain_trail = False
        if ball.position == self.first_hit_position:
            ball.speed = ball.first_hit_speed
        elif ball.position == self.position:
            ball.speed = ball.perfect_hit_speed
            ball.plain_trail = True
        elif self.direction == 1 and ball.position < 3:
            ball.speed = ball.late_hit_speed
        elif self.direction == -1 and ball.position > self.position - 3:
            ball.speed = ball.late_hit_speed
        else:
            ball.speed = 1

        return True

    def get_dot_color(self):
        return self.bonus.get_color() if self.bonus else self.color
