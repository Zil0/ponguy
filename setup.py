#!/usr/bin/env python
from setuptools import setup

setup(
    name='ponguy',
    version='dummy',
    description='Dummy package for CI',
    include_package_data=True,
    py_modules=['ponguy'],
)
