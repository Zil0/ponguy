from ponguy.animation import RandomColors
from ponguy.strip import Strip


def test_animation_done(freezer):
    strip = Strip()
    animation = RandomColors(strip)
    animation.duration_ms = 10
    animation.init_anim()

    assert animation.done() is False

    freezer.tick(5 * 1000)
    assert animation.done() is False

    freezer.tick(6 * 1000)
    assert animation.done() is True
