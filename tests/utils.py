from contextlib import contextmanager
from unittest import mock

import machine

from ponguy import Colors, config
from ponguy.game import Game
from ponguy.player import Player
from ponguy.strip import START_HEADER_SIZE, Strip


def make_playing_game(opposite_player_sends=False):
    strip = Strip()
    players = [
        Player(machine.Pin(1, machine.Pin.IN, machine.Pin.PULL_UP), Colors.RED, 0, 1),
        Player(
            machine.Pin(2, machine.Pin.IN, machine.Pin.PULL_UP),
            Colors.BLUE,
            config.NUM_LEDS - 1,
            -1,
        ),
    ]
    game = Game(strip, players)

    game.sender = players[0] if not opposite_player_sends else players[1]
    game.init_game()
    game.start_pingpong()

    return game


@contextmanager
def mock_button_press(player):
    def mock_value(self, on_off=None):
        if self.no == player.button_pin.no:
            return 0

    with mock.patch('machine.Pin.value', mock_value):
        yield


def get_pixel_color(game, i):
    index = i * 4 + START_HEADER_SIZE
    return tuple(reversed(game.strip._buf[index + 1 : index + 4]))


def get_range_color(game, start, end):
    color = get_pixel_color(game, start)

    if any(get_pixel_color(game, i) != color for i in range(start + 1, end)):
        raise ValueError('range contains multiple colors')

    return color
